<?php
namespace App\Repositories\LinkGenerator\Contract;

use App\Models\Link;
use App\Models\LinkGenerator;

interface LinkGeneratorRepositoryInterface
{
public function store(Link $link): LinkGenerator;
}
