<?php

namespace App\Repositories\LinkGenerator;

use App\Models\Link;
use App\Models\LinkGenerator;
use App\Repositories\Link\Contract\LinkRepositoryInterface;
use App\Repositories\LinkGenerator\Contract\LinkGeneratorRepositoryInterface;
use Illuminate\Support\Str;

class LinkGeneratorRepository implements LinkGeneratorRepositoryInterface
{

    /**
     * @param LinkRepositoryInterface $linkRepository
     * @return string
     */
    public function store(Link $link): LinkGenerator
    {
        $shortenedLink = Str::random(8);
        $newLinkGenerator = new LinkGenerator();
        $newLinkGenerator->shortened_link = $shortenedLink;
        $newLinkGenerator->link_id = $link->id;
        $newLinkGenerator->save();

        return $newLinkGenerator;
    }
}
