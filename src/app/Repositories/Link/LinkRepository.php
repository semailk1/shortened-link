<?php
namespace App\Repositories\Link;

use App\Models\Link;
use App\Models\LinkGenerator;
use App\Repositories\LinkGenerator\Contract\LinkGeneratorRepositoryInterface;
use Illuminate\Http\Request;
use App\Repositories\Link\Contract\LinkRepositoryInterface;

class LinkRepository implements LinkRepositoryInterface
{
    /**
     * @var LinkGeneratorRepositoryInterface
     */
    private $generatorRepository;

    public function __construct(LinkGeneratorRepositoryInterface $generatorRepository)
    {
        $this->generatorRepository = $generatorRepository;
    }

    /**
     * @param Request $request
     * @return LinkGenerator
     */
    public function store(Request $request): LinkGenerator
    {
        $newLink = new Link();
        $newLink->link = $request->get('link');
        $newLink->save();


        $shortenedLink = $this->generatorRepository->store($newLink);

        return $shortenedLink;
    }
}
