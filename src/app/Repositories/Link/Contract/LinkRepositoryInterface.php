<?php

namespace App\Repositories\Link\Contract;

use App\Models\LinkGenerator;
use Illuminate\Http\Request;

interface LinkRepositoryInterface
{
    public function store(Request $request): LinkGenerator;
}
