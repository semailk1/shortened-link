<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $counter
 * @property int $link_id
 * @property string $shortened_link
 *
 * Class LinkGenerator
 * @package App\Models
 */
class LinkGenerator extends Model
{
    use HasFactory;

    protected $fillable = ['counter', 'link_id', 'shortened_link'];

    /**
     * Scope a query to only include active users.
     *
     * @param Builder $query
     * @return void
     */
    public function scopeFindLink($query, $link)
    {
        $query->where('shortened_link', '=', $link);
    }

    /**
     * @return Attribute
     */
    public function shortenedLink(): Attribute
    {
        return Attribute::make(
            function ($value) {
                return 'localhost:8183/api/index/' . $value;
            }
        );
    }

    /**
     * @return BelongsTo
     */
    public function link(): BelongsTo
    {
        return $this->belongsTo(Link::class);
    }
}
