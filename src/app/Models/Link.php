<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $link
 *
 * Class Link
 * @package App\Models
 */
class Link extends Model
{
    use HasFactory;

    protected $fillable = ['link'];

    /**
     * @return HasOne
     */
    public function linkGenerator(): HasOne
    {
        return $this->hasOne(LinkGenerator::class);
    }
}
