<?php

namespace App\Providers;

use App\Repositories\Link\LinkRepository;
use App\Repositories\LinkGenerator\Contract\LinkGeneratorRepositoryInterface;
use App\Repositories\LinkGenerator\LinkGeneratorRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Link\Contract\LinkRepositoryInterface;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(LinkRepositoryInterface::class, LinkRepository::class);
        $this->app->bind(LinkGeneratorRepositoryInterface::class, LinkGeneratorRepository::class);
    }
}
