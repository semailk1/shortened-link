<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Link;
use App\Models\LinkGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Link\Contract\LinkRepositoryInterface;

class LinkGeneratorController extends Controller
{
    /**
     * @param string $link
     * @return JsonResponse
     */
    public function index(string $link): JsonResponse
    {
        /** @var LinkGenerator $linkGenerator */
        $linkGenerator = LinkGenerator::findLink($link)->first();
        $linkGenerator->counter = ++$linkGenerator->counter;
        $linkGenerator->save();

        $linkGenerator->link;

        return \response()->json($linkGenerator, Response::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function links(): JsonResponse
    {
        $links = Link::with('linkGenerator')->get();

        return \response()->json($links, Response::HTTP_OK);
    }

    /**
     * @var LinkRepositoryInterface
     */
    private $linkRepository;

    public function __construct(LinkRepositoryInterface $linkRepository)
    {
        $this->linkRepository = $linkRepository;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function shortenedLinkAction(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), ['link' => 'required|unique:links']);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }
        $linkCreate = $this->linkRepository->store($request);

        return response()->json($linkCreate->shortened_link, Response::HTTP_CREATED);
    }
}
