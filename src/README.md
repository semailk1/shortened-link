1. Клонируем репазиторий.
2. В корне проекта поднимаем контейнера (docker-compose up -d)
3. Заходим внутрь контейнера (docker-compose exec --user=1000 php sh)
4. Подтягиваем зависести composer update
5. Создаем .env файл (cp .env.example .env)
6. php artisan key:generate
7. настраиваем подключение к базе данных: <br> 
        7.1 MYSQL_DATABASE: homestead <br>
        7.2 MYSQL_USER: homestead <br>
        7.3 MYSQL_PASSWORD: secret
8. php artisan migrate
9. Route::post (shortened-link) передаем обязательный параметр link в котором содержиться ссылка и получаем сокращенную ссылку.
10. Route::get (index/{link}) в который передаем короткую ссылку. Получаем обе ссылки и увеличиваем счетчик.
11. Route::get (links) получаем все ссылки
