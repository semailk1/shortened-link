<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_generators', function (Blueprint $table) {
            $table->id();
            $table->foreignId('link_id')->constrained('links')->references('id')->cascadeOnDelete();
            $table->unsignedBigInteger('counter')->default(0);
            $table->string('shortened_link')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_generators');
    }
};
