<?php

use App\Http\Controllers\Api\LinkGeneratorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('shortened-link', [LinkGeneratorController::class, 'shortenedLinkAction']);
Route::get('index/{link}', [LinkGeneratorController::class, 'index']);
Route::get('links', [LinkGeneratorController::class, 'links']);
